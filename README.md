# Pairing Exercise #

Thank you for taking the time to work on this task with us. We have
scheduled an hour and a half. The first 15 minutes is for us
to discuss the problem and come up with a solution. This is
a collaborative task so your partner is there to help so feel free to
ask questions and get them to assist you. Then we will work on the
solution for the next hour, it's not a problem if we do not have a fully
working solution at the end of the alloted time. The final fifteen
minutes will be where we review the solution and discuss what
improvements and optimisations would be needed to make the code ready
for production.

### The problem  ###

Sun Racing is striving to be the go to destination for horse racing fans. One
thing clued up punters look for is the weather on race days. For example heavy rain
means heavy ground and this is a major factor in how different horses
perform.

We have been asked to build a service which will allow us to display
accurate weather information for each UK + IE racecourse. We should be
able to call the service with a course name and a ISO8601 datetime and get back
the text forecast, temperature and rainfall.

A .json file containing a subset of UK + IE racecourses and their
coordinates can be found in this repo.

### Requirements ###

* Weather can be queried by racecourse + datetime over http
* Uses the open [Open Weather API](https://openweathermap.org/forecast5) (an
  API key will be provided)
* Uses metric units
* Returns JSON
* Node.js - vanilla .js or typescript

### Notes ###

* The openweathermap.org API only returns the next 5 days of data split into
  3 hour intervals
* You can use any packages you like to help you build you solution
